<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.1
 */

get_header();
	 // if ( have_posts() ) while ( have_posts() ) : the_post();
	 woocommerce_output_content_wrapper();
?>
			<article <?php post_class(); ?> >
				<h1>Erro 404 - <?php _e( 'Página não encontrada', 'twentyten' ); ?></h1>
				<p><?php _e( 'Não encontramos a página que você procura. Use a barra de busca para tentar encontrar.', 'twentyten' ); ?></p>				

				<script type="text/javascript">
					// focus on search field after it has loaded
					// document.getElementsByClassName('orig') && document.getElementsByClassName('orig').focus();
				</script>

			</article>
<?php 
	// endwhile; // end of the loop.
	woocommerce_output_content_wrapper_end();
get_footer(); 
?>