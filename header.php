<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.1
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link href="<?php echo get_template_directory_uri()	?>/assets/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />

<!-- <link href='https://fonts.googleapis.com/css?family=' rel='stylesheet' type='text/css'> -->
<link href="https://fonts.googleapis.com/css?family=Exo+2:200,400,400i,500,600,700,800|Work+Sans:400,500" rel="stylesheet">
<!-- google maps -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<?php
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
	wp_head();
?>
</head>

<body <?php body_class(); ?>>
	
	<script>
	  window.fbAsyncInit = function() {
	    FB.init({
	      appId      : '199814650350930',
	      xfbml      : true,
	      version    : 'v2.5'
	    });
	  };

	  (function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
	     js.src = "//connect.facebook.net/en_US/sdk.js";
	     fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	</script>

	<header id="header">			
		<div id="stick-menu">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<?php 
							$current_user = wp_get_current_user();
							$userName = ($current_user->display_name)? $current_user->display_name : 'Visitante' ;
							if (is_user_logged_in()) {
								echo '<p>Bem vindo, <strong>'. $userName.'</strong>. Boas compras! </p>';
							} else {
								echo '<p>Bem vindo, <strong>Visitante</strong>. <a href="'. get_permalink( get_option('woocommerce_myaccount_page_id') ) .'">Registre-se</a> ou <a href="'. get_permalink( get_option('woocommerce_myaccount_page_id') ) .'">faça login</a>.</p>';
							}
							
						?>
					</div>
					<div class="col-sm-6 text-right hidden-xs">
						<p class="h6"><?php the_field('mensagem_topo', 'option') ?></p>
					</div>
				</div>
			</div>
		</div>
		<div id="main-menu">
			<div class="container">
				<div class="row" id="menu-interno">

					<div class="col-sm-3">
						<a id="logotipo" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
							<img src="<?php echo get_field('logotipo', 'option'); ?>" class="logotipo img-responsive"/>
						</a>
					</div>

					<div class="col-sm-9">
						<div id="menu-usuario" class="pull-right">
				    	<?php wp_nav_menu( array( 'theme_location'    => 'usuario' )); ?>    
						</div>						
						<div class="clearfix"></div>
						
						<div class="row box-busca">														
							<div class="col-sm-7 col-md-8 col-lg-9"><?php echo do_shortcode('[wpdreams_ajaxsearchpro id=1]'); ?></div>
							<div class="col-sm-5 col-md-4 col-lg-3 hidden-xs">
								<a class="telefone_info"  title="Ligue para nós!" href="tel:<?php $televendas = get_field('telefone_info','option'); echo $televendas; ?>">
									<span>FALE CONOSCO</span>
									<?php echo mascara_string('(##) ####-####',$televendas); ?>
								</a>
							</div>							
						</div>
					</div>
									
				</div> <!-- row -->	

					<div class="row">
						<div class="col-sm-4 col-md-3 linha-produtos hidden-xs">
							<span>LINHA DE PRODUTOS</span>
						</div>
						<div class="col-sm-8 col-md-9">
							<nav id="navmenu" class="navbar navbar-default" role="navigation">
							  <div class="navbar-header">
							    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".menu-retratil">
							      <span class="sr-only">Toggle navegação</span>
							      <span class="icon-bar"></span>
							      <span class="icon-bar"></span>
							      <span class="icon-bar"></span>
							    </button>
							    <a class="navbar-brand visible-xs" href="#">Menu</a>
							  </div>
					    	<?php 
					    		wp_nav_menu( array( 						    		
		    		        'theme_location'    => 'global',
		    		        'depth'             => 2,
		    		        'container'         => 'div',
		    		        'container_class'   => 'collapse navbar-collapse menu-retratil',
		    		        'menu_class'        => 'nav navbar-nav',
		    		        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
		    		        'walker'            => new wp_bootstrap_navwalker()
					    		)); 
					    	?>    
							</nav><!-- /.navbar-collapse -->
						</div>
					</div>
				</div>

			</div> <!-- CONTAINER --> 			
		
	</header>



