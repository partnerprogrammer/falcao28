<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.1
 */
	echo '<aside class="col-sm-4 col-md-3">';
		echo get_template_part('partials/_menu-local' );
		if ( is_active_sidebar( 'sidebar-principal' ) ) :					
				if (!is_front_page()) { echo get_template_part('partials/_banners' ); }
				do_action( 'woocommerce_sidebar' );
				dynamic_sidebar( 'sidebar-principal' );					
		endif;
	echo '</aside>';
