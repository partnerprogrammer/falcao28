<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the wordpress construct of pages
 * and that other 'pages' on your wordpress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.1
 */

get_header();
	 if ( have_posts() ) while ( have_posts() ) : the_post();
	 woocommerce_output_content_wrapper();
?>
			<article <?php post_class('' ); ?> >
				<header> <?php woocommerce_breadcrumb(); ?> </header>
				
				<section class='conteudo'>
					<?php the_content(); ?>
				</section>
				
			</article>
<?php 
	endwhile; // end of the loop.
	woocommerce_output_content_wrapper_end();
get_footer(); 
?>
