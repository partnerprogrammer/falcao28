jQuery(document).ready(function($){ 

	$('select:not(.country_select, .state_select, #rating)').wrap('<div class="select-box"></div>');

	$( "ul#menu-linha-de-produtos li.menu-item-has-children" ).mouseenter(function() { 
				$(this).children('ul.sub-menu').collapse('show');						
		}).mouseleave(function() {			
				$(this).children('ul.sub-menu').collapse('hide');	
	});

  //Banner
  $('.banner').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
  });

  // Slider responsivo
	$('.responsivo').slick({
		dots: true,
		arrows: false,
		infinite: false,
		speed: 500,
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
				{
					breakpoint: 1024,
					settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: true
				}
			},
				{
					breakpoint: 600,
					settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
				{
					breakpoint: 480,
					settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
});