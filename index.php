<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query. 
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.1
 */

get_header(); ?>
	<div id="content">
		<div id="slider-box">
				<div class="container">
					<div class="row">
						<aside class="col-sm-4 col-md-3">
							<?php echo get_template_part('partials/_menu-local' ); ?>	
						</aside>					
						<div class="col-sm-8 col-md-9">
							<div class="row">
								<div class="col-xs-12">
										<?php if( have_rows('slider_principal', 10) ): ?>
											<section class="banner" role="banner">
												<?php  
													while ( have_rows('slider_principal', 10) ) : the_row();												
												  $url = get_sub_field('imagem_slider');												  
												?>
												
													<div>
														<a href="<?php the_sub_field('link_slider') ?>" title="Veja mais">
															<img src="<?php echo $url; ?>" class="img-responsive" />
														</a>														
													</div>
												  
												<?php endwhile; ?>
											</section>
										<?php endif; ?>										
								</div>
							</div> <!-- row -->
							<?php echo get_template_part('partials/_banners' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	
	<div class="container">		
		<?php echo get_template_part('partials/_newsletter') ?>	
	</div>

	<ul class="nav nav-tabs text-center" role="tablist">		
	  <li role="presentation" class="active"><a aria-controls="recentes" role="tab" data-toggle="tab" href="#recentes">PRODUTOS RECENTES</a></li>
	  <li role="presentation"><a aria-controls="promocoes" role="tab" data-toggle="tab" href="#promocoes">EM PROMOÇÃO</a></li>
	  <li role="presentation"><a aria-controls="destaques" role="tab" data-toggle="tab" href="#destaques">PRODUTOS EM DESTAQUE</a></li>
	</ul>
	
	<div class="tab-content container">
		<div id="recentes" role="tabpanel" class="tab-pane active"><?php echo do_shortcode('[recent_products per_page="8" columns="4" ]') ?></div>
		<div id="promocoes" role="tabpanel" class="tab-pane"><?php echo do_shortcode('[sale_products per_page="8" columns="4" ]') ?></div>
		<div id="destaques" role="tabpanel" class="tab-pane"><?php echo do_shortcode('[featured_products per_page="8" columns="4" ]') ?></div>
	</div>
	
<?php get_footer(); ?>
