<?php
/**
 * Template Name: Fale Conosco
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.1
 */

get_header();
	 if ( have_posts() ) while ( have_posts() ) : the_post();
	 woocommerce_output_content_wrapper();
?>
			<article <?php post_class('fale-conosco' ); ?> >
				<header> <?php woocommerce_breadcrumb(); ?> </header>
				
				<div class="row">
					<div class="col-sm-6">
						<!-- <header>
							<h1 class="page-title"><?php the_title(); ?></h1>
						</header>		 -->
						<div class="conteudo">
							<?php the_content(); ?>
						</div>
						<div id="contato">
							<?php echo do_shortcode('[contact-form-7 id="202" title="Fale Conosco"]'); ?>
						</div>
					</div>
					<div class="col-sm-6">
						<?php 
							$location = get_field('mapa_info','option');
							if( !empty($location) ):
						?>
							<div class="acf-map">
								<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
							</div>
						<?php endif; ?>
						<h4><strong>Onde Estamos</strong></h4><br>
						<p><?php the_field('endereco_info', 'option') ?></p>
					</div>
				</div>

			</article>
<?php 
	endwhile; // end of the loop.
	woocommerce_output_content_wrapper_end();
get_footer(); 
?>