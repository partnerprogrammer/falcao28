<?php

remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

// Remove o aviso de cupom no checkout
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );


add_filter( 'woocommerce_get_price_html', 'wpa83367_price_html', 100, 2 );
function wpa83367_price_html( $price, $product ){
		if ($product->is_on_sale()) {
			return '<span class="from"> De: ' . str_replace( '<ins>', '</span> por: <ins>', $price );
		} else {
			return 'por: ' . $price;
		}		    
}


// Remove each style one by one
add_filter( 'woocommerce_enqueue_styles', 'jk_dequeue_styles',100 );
function jk_dequeue_styles( $enqueue_styles ) {
	unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
	unset( $enqueue_styles['woocommerce-layout'] );		// Remove the layout
	unset( $enqueue_styles['woocommerce-smallscreen'] );	// Remove the smallscreen optimisation
	return $enqueue_styles;
}


function your_woo_ajax_solution( $translation, $text, $domain ) {
	if (!is_single()) {
		if ( $domain == 'woocommerce' ) { // your domain name
		    if ( $text == 'View Cart' ) { // current text that shows
		        $translation = "Adicionado! Veja o <i class='fa fa-cart-plus'></i>"; // The text that you would like to show
		    }
		  }
		  return $translation;
	} else {
		return $translation;
	}
}
// add_filter( 'gettext', 'your_woo_ajax_solution', 10, 3 );


function force_pretty_displaynames($user_login, $user) {

    $outcome = trim(get_user_meta($user->ID, 'first_name', true) . " " . get_user_meta($user->ID, 'last_name', true));
    if (!empty($outcome) && ($user->data->display_name!=$outcome)) {
        wp_update_user( array ('ID' => $user->ID, 'display_name' => $outcome));    
    }
}
add_action('wp_login','force_pretty_displaynames',10,2); 

// Or just remove them all in one line
// add_filter( 'woocommerce_enqueue_styles', '__return_false' );

/* Parcelamento */
/* ----------------------------------------- */
	/**
	 * Calculates the price in 3 installments without interest.
	 *
	 * @return string Price in 3 installments.
	 */
	function cs_product_parceled() {
			// $product = get_product();
			$product = wc_get_product();
			$price = wc_get_price_including_tax($product);
	    if ( $price ) {
	        $value = wc_price( $price / 3 );
	        return $value;
	    }
	}
	/**
	 * Displays the Installments on product loop.
	 * 
	 * @return string Price in 3 installments.
	 */
	function cs_product_parceled_loop() {
	    echo '<span style="color: #666; font-size: 100%" class="price parcelado">' . __( '3x de' ) . ' ' . cs_product_parceled() . ' <span class="normal">iguais</span></span>';
	}
	/**
	 * Displays the Installments on the product page.
	 *
	 * @return string Price in 3 installments.
	 */
	function cs_product_parceled_single() {
	    // $product = get_product();
	    $product = wc_get_product();
	?>
	    <div itemprop="offers" class="offers" itemscope itemtype="http://schema.org/Offer">

	              
	        <span class="price" itemprop="price"><?php echo $product->get_price_html(); ?> à vista</span>
	        <span class="parcelado"><?php  _e( 'ou 3x de' ) ?> <?php echo cs_product_parceled(); ?> <span class="normal">iguais</span></span>
	        
	        <meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
	        <link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
	    </div>
	<?php
	}
	add_action( 'woocommerce_after_shop_loop_item_title', 'cs_product_parceled_loop', 20 );
	
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
	add_action( 'woocommerce_single_product_summary', 'cs_product_parceled_single', 8 );


	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 6 );

/* ----------------------------------------- Parcelamento */		

/* Content Loop */
/* ----------------------------------------- */
	function wpa89819_wc_single_product(){

	    $product_cats = wp_get_post_terms( get_the_ID(), 'product_cat' );

	    if ( $product_cats && ! is_wp_error ( $product_cats ) ){

	        $single_cat = array_shift( $product_cats ); ?>

	        <h5 itemprop="name" class="product_category_title"><span><?php echo $single_cat->name; ?></span></h5>

	<?php }
	}
	add_action( 'woocommerce_before_shop_loop_item_title', 'wpa89819_wc_single_product', 20 );


/* ----------------------------------------- Content Loop */		

// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns', 100);
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}




function custom_woocommerce_product_add_to_cart_text( $text, $product ) {
  switch ( $product->get_type() ) {
    case 'simple':
      return __( 'ADICIONAR AO CARRINHO' );
      break;
    case 'variable':
      return __( 'VEJA AS OPÇÕES' );
      break;
    // case 'grouped':
    //   return __( 'Texto para grupo de produtos' );
    //   break;
    default:
      return $text;
      break;
  }
}
add_filter( 'woocommerce_product_add_to_cart_text', 'custom_woocommerce_product_add_to_cart_text', 10, 2 );

