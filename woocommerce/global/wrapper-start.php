<?php
/**
 * Content wrappers
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
} ?>

	<div id="content">
		<div class="container">
			<div class="row">
				<?php get_sidebar(); ?>
				<div class="col-sm-8 col-md-9 box-store columns-3">

