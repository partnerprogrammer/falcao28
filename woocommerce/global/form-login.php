<?php
/**
 * Login form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( is_user_logged_in() ) {
	return;
}

?>
<form method="post" class="login form-horizontal" <?php if ( $hidden ) echo 'style="display:none;"'; ?>>

	<?php do_action( 'woocommerce_login_form_start' ); ?>

	<?php if ( $message ) echo wpautop( wptexturize( $message ) ); ?>
	
	<div class="form-group">
		<label class="col-sm-3 control-label" for="username"><?php _e( 'Username or email', 'woocommerce' ); ?> <span class="required">*</span></label>
		<div class="col-sm-9">
			<input type="text" class="input-text form-control" name="username" id="username" />	
		</div>		
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label" for="password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
		<div class="col-sm-9">
			<input class="input-text form-control" type="password" name="password" id="password" />
		</div>
	</div>

	<div class="form-group">
	    <div class="col-sm-offset-3 col-sm-9">
	      <div class="checkbox">
	        <label for="rememberme" class="inline">
	        	<input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php _e( 'Remember me', 'woocommerce' ); ?>
	        </label>  |   <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php _e( 'Lost your password?', 'woocommerce' ); ?></a>

	      </div>
	    </div>
	  </div>
	  <div class="form-group">
	    <div class="col-sm-offset-3 col-sm-9">
	      <?php wp_nonce_field( 'woocommerce-login' ); ?>
    		<input type="submit" class="button" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>" />
    		<input type="hidden" name="redirect" value="<?php echo esc_url( $redirect ) ?>" />		
	    </div>
	  </div>

	<?php do_action( 'woocommerce_login_form' ); ?>
	<?php do_action( 'woocommerce_login_form_end' ); ?>

</form>
