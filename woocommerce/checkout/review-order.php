<?php
/**
 * Review order table
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="shop_table woocommerce-checkout-review-order-table">
	<h2><?php _e( 'Cart Totals', 'woocommerce' ); ?></h2>
	
	<p class="cart-subtotal">
		<?php _e( 'Subtotal', 'woocommerce' ); ?>
		<span class="pull-right">
			<?php wc_cart_totals_subtotal_html(); ?>
		</span>
	</p>

	<p class="frete">
		Selecione o frete <br>
		<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
			<?php  do_action( 'woocommerce_cart_totals_before_shipping' ); ?>
			<?php wc_cart_totals_shipping_html(); ?>
			<?php do_action( 'woocommerce_cart_totals_after_shipping' ); ?>
		<?php endif; ?>
	</p>
	
	<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
		<p class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
			<?php wc_cart_totals_coupon_label( $coupon ); ?>
			<span class="pull-right">
				<?php wc_cart_totals_coupon_html( $coupon ); ?>
			</span>
		</p>
	<?php endforeach; ?>

	<hr style="margin: 10px 0;">	
	<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>
	<p class="order-total">
		<strong>TOTAL DO PEDIDO</strong>
		<span class="pull-right">
			<?php wc_cart_totals_order_total_html(); ?>
		</span>
	</p>
	<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>

</div>