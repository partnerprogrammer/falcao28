<?php
/**
 * Single Product title
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<nav id="product_nav">
	<ul>
		<span class="title">NAVEGUE</span>
		<li><?php previous_post_link('%link', '<i class="fa fa-chevron-left"></i> <span>%title</span>'); ?></li>
		<li><?php next_post_link('%link', '<i class="fa fa-chevron-right"></i> <span>%title</span>'); ?></li>
	</ul>
</nav>

<h1 itemprop="name" class="product_title entry-title"><?php the_title(); ?></h1>