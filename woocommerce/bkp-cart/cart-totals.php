<?php
/**
 * Cart totals
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.6
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div class="cart_totals <?php if ( WC()->customer->has_calculated_shipping() ) echo 'calculated_shipping'; ?>">

	<?php do_action( 'woocommerce_before_cart_totals' ); ?>

	<h2><?php _e( 'Cart Totals', 'woocommerce' ); ?></h2>
	
	<p class="cart-subtotal">
		<?php _e( 'Subtotal', 'woocommerce' ); ?>
		<span class="pull-right">
			<?php wc_cart_totals_subtotal_html(); ?>
		</span>
	</p>

	<p class="frete">
		Selecione o frete <br>
		<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
			<?php  do_action( 'woocommerce_cart_totals_before_shipping' ); ?>
			<?php wc_cart_totals_shipping_html(); ?>
			<?php do_action( 'woocommerce_cart_totals_after_shipping' ); ?>
		<?php endif; ?>
	</p>

	<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
		<p class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
			<?php wc_cart_totals_coupon_label( $coupon ); ?>
			<span class="pull-right">
				<?php wc_cart_totals_coupon_html( $coupon ); ?>
			</span>
		</p>
	<?php endforeach; ?>
	
	<hr style="margin: 10px 0;">	
	<?php do_action( 'woocommerce_cart_totals_before_order_total' ); ?>
	<p class="order-total">
		<strong>TOTAL DO PEDIDO</strong>
		<span class="pull-right">
			<?php wc_cart_totals_order_total_html(); ?>
		</span>
	</p>
	<?php do_action( 'woocommerce_cart_totals_after_order_total' ); ?>


	<div class="wc-proceed-to-checkout">
		<hr>
		<input type="submit" class="button button-success-inverse" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'woocommerce' ); ?>" />
		<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>

	</div>
	
	<?php do_action( 'woocommerce_after_cart_totals' ); ?>

</div>
