<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 3 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// print($woocommerce_loop['columns']);
// die();

	if ($woocommerce_loop['columns'] == 4) {
		if ($woocommerce_loop['loop'] % 4 == 0) { echo '<div class="clearfix visible-md-block visible-lg-block"></div>';}
		if ($woocommerce_loop['loop'] % 2 == 0) { echo '<div class="clearfix visible-xs-block visible-sm-block"></div>';}
		$classes = 'col-xs-6 col-md-3';
	} else {
		// Vai usar o padrão do filtro no woo.php, 3 columas
		if ($woocommerce_loop['loop'] % 3 == 0) { echo '<div class="clearfix visible-md-block visible-lg-block"></div>';}
		if ($woocommerce_loop['loop'] % 2 == 0) { echo '<div class="clearfix visible-xs-block visible-sm-block"></div>';}
		$classes = 'col-xs-6 col-md-4';
	}

// Increase loop count
$woocommerce_loop['loop']++;

?>
<li class="<?php echo $classes; ?>">
	
	<div <?php post_class(); ?>>

		<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

			<?php
				
				/**
				 * woocommerce_shop_loop_item_title hook
				 *
				 * @hooked woocommerce_template_loop_product_title - 10
				 */
				do_action( 'woocommerce_shop_loop_item_title' );

				
				/**
				 * woocommerce_before_shop_loop_item_title hook
				 *
				 * @hooked woocommerce_show_product_loop_sale_flash - 10
				 * @hooked woocommerce_template_loop_product_thumbnail - 10
				 */
				do_action( 'woocommerce_before_shop_loop_item_title' );

				

				/**
				 * woocommerce_after_shop_loop_item_title hook
				 *
				 * @hooked woocommerce_template_loop_rating - 5
				 * @hooked woocommerce_template_loop_price - 10
				 */
				do_action( 'woocommerce_after_shop_loop_item_title' );
			?>

		<?php

			/**
			 * woocommerce_after_shop_loop_item hook
			 *
			 * @hooked woocommerce_template_loop_add_to_cart - 10
			 */
			do_action( 'woocommerce_after_shop_loop_item' );

		?>
	</div>
</li>
