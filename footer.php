<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.1
 */
?>
	<footer id="footer">
		<div id="primary-footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-3 col-md-2">
						<h4>ÁREA DO CLIENTE</h4>
						<?php wp_nav_menu( array( 'menu' => '30', 'depth' => 1, 'container' => 'div')); ?>
					</div>
					<div class="col-sm-3 col-md-3">
						<h4>LINHA DE PRODUTOS</h4>
						<?php wp_nav_menu( array( 'menu' => '29', 'depth' => 1, 'container' => 'div')); ?>
					</div>
					<div class="col-sm-3 col-md-2">
						<h4>INSTITUCIONAL</h4>
						<?php wp_nav_menu( array( 'menu' => '17', 'depth' => 1, 'container' => 'div')); ?>
					</div>
					<div class="col-sm-3 col-md-2">
						<h4>SOBREA  LOJA</h4>
						<?php wp_nav_menu( array( 'menu' => '31', 'depth' => 1, 'container' => 'div')); ?>
					</div>
					<div class="col-sm-12 col-md-3">
						<div style="margin: -1px 0px 0px -1px; " class="fb-page" data-href="https://www.facebook.com/falcao28militaria" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/falcao28militaria"><a href="https://www.facebook.com/falcao28militaria">Falcão 28 Artigos Militares</a></blockquote></div></div>													
					</div>				
				</div>
			</div>
		</div>

		<div id="secondary-footer">
			<div class="container">
				 <div class="row">
				 	<div class="col-sm-2 col-md-6">
				 			<h4> FORMAS DE PAGAMENTO <img class="pull-right" src="<?php the_field('formas_de_pagamento', 'option') ?>" alt="" class="img-responsive"> </h4>
				 	</div>
				 	<div class="col-sm-4 col-md-3">
				 		<a class="telefone_info"  title="Ligue para nós!" href="tel:<?php $televendas = get_field('telefone_info','option'); echo $televendas; ?>">
				 			<span>ATENDIMENTO</span>
				 			<?php echo mascara_string('(##) ####-####',$televendas); ?>
				 		</a>
				 	</div>
				 	<div class="col-sm-4 col-md-3">
				 		<a class="email_info" href="mailto:<?php $email=get_field('email_info','option'); echo $email; ?>?subject=Fale Conosco" title="<?php echo __('Fale Conosco', 'rca'); ?>">
				 			<span>VENDAS</span>
				 			<?php echo $email; ?>
				 		</a>	
				 	</div>
				 </div>
			</div>	
		</div>

		<div id="thirty-footer">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="texto-juridico">
							<?php the_field('informações_juridicas', 'option') ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<p class="copy">
							© <?php echo date('Y') ?> - <?php bloginfo('name'); ?> - Todos direitos reservados. <br>
							<?php echo get_field('endereco_info','option');?> <br>
							Atendimento: <?php echo mascara_string('(##) ####-####',$televendas); ?> | E-mail: <a class="email_info" href="mailto:<?php $email=get_field('email_info','option'); echo $email; ?>?subject=Fale Conosco" title="<?php echo __('Fale Conosco', 'rca'); ?>"><?php echo $email; ?></a>
						</p>
						<p class="text-right">
							<a href="http://bigodesign.com.br" target="_blank" id="bigo" title="Feito com espuma de barbear!"> desenvolvido por <strong>BIGO</strong></a>
						</p>
					</div>
				</div>

			</div>
		</div>

	</footer>
<?php wp_footer(); ?>
</body>
</html>

			    	