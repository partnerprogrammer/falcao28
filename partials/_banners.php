<?php if( have_rows('banners', 10) ): ?>
  <div class="sidebar-banners hidden-xs row">
    <?php  
      while ( have_rows('banners', 10) ) : the_row();                        
      $url = get_sub_field('imagem_slider');
      $titulo = get_sub_field('titulo');
      $chamada = get_sub_field('chamada');
      $cssClasses = (is_front_page()) ? 'col-md-4' : 'col-sm-12';
    ?>    
      <div class="banner-item <?php echo $cssClasses; ?>">
        <div class="mask" style="background-image: url('<?php echo $url; ?>');">
          <a href="<?php the_sub_field('link_slider') ?>" title="<?php echo $titulo; ?>">          
            <h2><?php echo $titulo; ?></h2>
            <span><?php echo $chamada; ?></span>
          </a>                              
        </div>        
      </div>
      
    <?php endwhile; ?>
  </div>
<?php endif; ?>