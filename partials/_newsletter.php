<div class="row" id="chamada-newsletter">
	<div class="col-sm-4">
		<img src="<?php images_url('newsletter.png') ?>" alt="Receba novidades em seu e-mail" class="img-responsive">
	</div>
	<div class="col-sm-8">
		<?php echo do_shortcode('[simplenewsletter]'); ?>
	</div>
</div>